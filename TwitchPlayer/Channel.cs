﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TwitchPlayer
{
    public class Channel
    {
        public string display_name { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public long followers { get; set; }
        public string status { get; set; }
    }
}
