﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Newtonsoft.Json;
using RestSharp;
using Vlc.DotNet.Core;
using Vlc.DotNet.Wpf;


namespace TwitchPlayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable
    {
        private string game;
        private string sig;
        private string token;
        private List<Stream> streamUrls;
        private string quality;
        private string streamName;
        private GridLength mainGridColumn1Length;
        VlcControl vlcPlayer;
        private GridLength mainGridColumn2Length;
        private double heigth;
        private double width;
        private bool streamSelected;

        public MainWindow()
        {
            InitializeComponent();
            gamePicker.ItemsSource = getGames().top;
            game = "StarCraft II: Heart of the Swarm";
            int index = ((List<Top>)gamePicker.Items.SourceCollection).FindIndex(p => p.game.name == game);
            gamePicker.SelectedIndex = index;
            listBox.ItemsSource = getStreams(game).streams;
            volumeSlider.Value = 100;

            initVlc();
        }

        /// <summary>
        /// Initialize VLC
        /// </summary>
        private void initVlc()
        {
            // Set libvlc.dll and libvlccore.dll directory path
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            path = System.IO.Path.GetDirectoryName(path);
            Vlc.DotNet.Core.VlcContext.LibVlcDllsPath = System.IO.Path.Combine(Environment.CurrentDirectory, @"vlc");
            // Set the vlc plugins directory path
            VlcContext.LibVlcPluginsPath = System.IO.Path.Combine(Environment.CurrentDirectory, @"plugins");

            // Set the startup options
            VlcContext.StartupOptions.IgnoreConfig = true;
            VlcContext.StartupOptions.LogOptions.LogInFile = false;
            VlcContext.StartupOptions.LogOptions.ShowLoggerConsole = false;
            VlcContext.StartupOptions.LogOptions.Verbosity = VlcLogVerbosities.Debug;
            VlcContext.StartupOptions.AddOption("--ffmpeg-hw");
            VlcContext.StartupOptions.AddOption("--ffmpeg-fast");

            VlcContext.StartupOptions.AddOption("--no-video-title-show");

            int cache = 1000;
            VlcContext.StartupOptions.AddOption("--udp-caching=" + cache);
            VlcContext.StartupOptions.AddOption("--tcp-caching=" + cache);
            VlcContext.StartupOptions.AddOption("--realrtsp-caching=" + cache);

            // Initialize the VlcContext
            VlcContext.Initialize();
            vlcPlayer = new VlcControl();

            // Add the VLC control to the grid to give it dimensions.
            // However VLC does not paint on its control geometry
            // instead it paints on its "VideoSource" image
            display.Children.Add(vlcPlayer);

            // WPF dark magic ahead: run-time interpreted data binding
            // When the VLC video changes (is loaded for example), the grid displays the new video image
            Binding vlcBinding = new Binding("VideoSource");
            vlcBinding.Source = vlcPlayer;

            // VLC paints into a WPF image
            Image vImage = new Image();
            vImage.SetBinding(Image.SourceProperty, vlcBinding);

            // The WPF image is used by a WPF brush
            VisualBrush vBrush = new VisualBrush();
            vBrush.TileMode = TileMode.None;
            vBrush.Stretch = Stretch.Uniform;
            vBrush.Visual = vImage;

            // The WPF brush is used by the grid element background
            display.Background = vBrush;
        }

        /// <summary>
        /// Gets list of streams based on current game.
        /// </summary>
        /// <param name="game">Currently selected game</param>
        /// <returns></returns>
        public Root getStreams(string game)
        {
            var client = new RestClient("https://api.twitch.tv/");

            var request = new RestRequest("kraken/streams", Method.GET);
            request.AddParameter("game", game);

            // easily add HTTP Headers
            request.AddHeader("Accept", "application/vnd.twitchtv.v2+json");


            // execute the request
            var response = client.Execute<Root>(request);
            return response.Data;
        }

        /// <summary>
        /// Gets list of 50 most popular games.
        /// </summary>
        /// <returns></returns>
        public Games getGames()
        {
            var client = new RestClient("https://api.twitch.tv/");

            var request = new RestRequest("kraken/games/top?limit=50&offset=0", Method.GET);

            // easily add HTTP Headers
            request.AddHeader("Accept", "application/vnd.twitchtv.v2+json");


            // execute the request
            var response = client.Execute<Games>(request);
            return response.Data;
        }
        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            runGamesWorker();
            runStreamsWorker();
        }

        /// <summary>
        /// Runs background worker which uses getGames() method.
        /// </summary>
        private void runGamesWorker()
        {
            BackgroundWorker gamesWorker = new BackgroundWorker();
            if (gamesWorker.IsBusy == false)
            {
                gamesWorker.DoWork += getGamesWorker_DoWork;
                gamesWorker.RunWorkerCompleted += getGamesWorker_RunWorkerCompleted;
                gamesWorker.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Runs background worker which uses getStreams() method.
        /// </summary>
        private void runStreamsWorker()
        {
            BackgroundWorker streamsWorker = new BackgroundWorker();
            if (streamsWorker.IsBusy == false)
            {
                streamsWorker.DoWork += getStreamsWorker_DoWork;
                streamsWorker.RunWorkerCompleted += getStreamsWorker_RunWorkerCompleted;
                streamsWorker.RunWorkerAsync(game);
            }
        }

        private void getGamesWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = getGames().top;
        }

        private void getGamesWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            List<Top> games = (List<Top>)e.Result;
            gamePicker.ItemsSource = games;
        }

        private void getStreamsWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            string game = e.Argument as string;
            e.Result = getStreams(game).streams;
        }

        private void getStreamsWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            List<Streams> streams = (List<Streams>)e.Result;
            listBox.ItemsSource = streams;
        }

        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBox.SelectedItem != null)
            {
                // playButton.IsEnabled = true;
                playerControls.Visibility = Visibility.Visible;
                streamName = ((Streams)listBox.SelectedItem).channel.name;
                getTokens(streamName);
                streamUrls = getStreamURL(streamName);
                streamSelected = true;
                qualityComboBox.ItemsSource = streamUrls;
                streamSelected = true;
                qualityComboBox.SelectedIndex = 0;

                vlcPlayer.Media = new Vlc.DotNet.Core.Medias.PathMedia((((Stream)streamUrls[0]).url));
                toggleStreamList();

                webBrowser1.Navigate("http://www.twitch.tv/" + streamName + "/chat");
            }
        }

        private void gamePicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gamePicker.SelectedItem == null)
            {
                int index = ((List<Top>)gamePicker.Items.SourceCollection).FindIndex(p => p.game.name == game);
                gamePicker.SelectedIndex = index;
            }
            game = ((Top)gamePicker.SelectedItem).game.name;
            runStreamsWorker();
        }

        /// <summary>
        /// Download access tokens 
        /// </summary>
        /// <param name="channel">Selected channel</param>
        private void getTokens(string channel)
        {
            using (var webClient = new System.Net.WebClient())
            {
                var URL = "http://api.twitch.tv/api/channels/" + channel + "/access_token";
                try
                {
                    var json = webClient.DownloadString(URL);

                    JsonTextReader reader = new JsonTextReader(new System.IO.StringReader(json));
                    while (reader.Read())
                    {
                        if (reader.Value != null)
                        {
                            if (reader.Value.Equals("sig"))
                            {
                                reader.Read();
                                if (reader.Value != null)
                                    sig = (string)reader.Value;
                                else
                                    continue;
                            }

                            if (reader.Value.Equals("token"))
                            {
                                reader.Read();
                                if (reader.Value != null)
                                    token = (string)reader.Value;
                                else
                                    continue;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Can't connect to server", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Gets url of stream.
        /// </summary>
        /// <param name="channel">Channel name</param>
        /// <returns></returns>
        private List<Stream> getStreamURL(string channel)
        {
            using (var webClient = new System.Net.WebClient())
            {
                if (sig != null && token != null)
                {
                    var url = "http://usher.twitch.tv/select/" + channel + ".json?nauthsig=" + sig + "&nauth=" + token + "&allow_source=true";
                    string text = webClient.DownloadString(url);

                    using (StringReader sr = new StringReader(text))
                    {
                        string line;
                        string name;
                        List<Stream> urls = new List<Stream>();
                        while ((line = sr.ReadLine()) != null)
                        {

                            if (line.StartsWith("#EXT-X-MEDIA:TYPE=VIDEO"))
                            {
                                string startString = "NAME=\"";
                                string endString = "\",AUTOSELECT";
                                int start = line.IndexOf(startString, 0) + startString.Length;
                                int end = line.IndexOf(endString, start);
                                name = line.Substring(start, end - start);

                                sr.ReadLine();
                                line = sr.ReadLine();
                                urls.Add(new Stream(line, name));
                            }
                        }
                        return urls;
                    }
                }
            }
            return null;
        }

        private void hideStreamsListButton_Click(object sender, RoutedEventArgs e)
        {
            toggleStreamList();
        }

        /// <summary>
        /// Toggle visibility of streamList.
        /// </summary>
        private void toggleStreamList()
        {
            if (streamList.Visibility == Visibility.Visible)
            {
                hideStreamsList();
            }
            else
            {
                showStreamsList();
            }
        }

        /// <summary>
        /// Shows streamList.
        /// </summary>
        private void showStreamsList()
        {
            streamList.Visibility = Visibility.Visible;
            mainGridColumn1.Width = mainGridColumn1Length;
            hideStreamListButton.Content = "<";
            mainGridColumn1.MinWidth = 150;
        }

        /// <summary>
        /// Hides streamList.
        /// </summary>
        private void hideStreamsList()
        {
            streamList.Visibility = Visibility.Collapsed;
            mainGridColumn1Length = mainGridColumn1.Width;
            mainGridColumn1.Width = new GridLength(0);
            var length = new GridLength(10, GridUnitType.Star);
            videoColumn.Width = length;
            hideStreamListButton.Content = ">";
            mainGridColumn1.MinWidth = 0;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            VlcContext.CloseAll();
        }

        private void hideChatButton_Click(object sender, RoutedEventArgs e)
        {
            toggleChat();
        }

        /// <summary>
        /// Toggle visibility of chat.
        /// </summary>
        private void toggleChat()
        {
            if (chat.Visibility == Visibility.Visible)
            {
                hideChat();
            }
            else
            {
                showChat();
            }
        }

        /// <summary>
        /// Shows chat.
        /// </summary>
        private void showChat()
        {
            chat.Visibility = Visibility.Visible;
            mainGridColumn2.Width = mainGridColumn2Length;
            hideStreamListButton.Content = "<";
            hideChatButton.Content = ">";
            mainGridColumn2.MinWidth = 250;
        }

        /// <summary>
        /// Hides chat.
        /// </summary>
        private void hideChat()
        {
            chat.Visibility = Visibility.Collapsed;
            mainGridColumn2Length = mainGridColumn2.Width;
            mainGridColumn2.Width = new GridLength(0);
            var length = new GridLength(10, GridUnitType.Star);
            videoColumn.Width = length;
            hideStreamListButton.Content = ">";
            hideChatButton.Content = "<";
            mainGridColumn2.MinWidth = 0;
        }

        private void display_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (vlcPlayer != null)
            {
                if (e.ClickCount == 2)
                {
                    toggleFullScreen();
                }
            }
        }

        /// <summary>
        /// Toggle full screen.
        /// </summary>
        private void toggleFullScreen()
        {
            if (WindowStyle != WindowStyle.None)
            {
                this.WindowState = WindowState.Maximized;
                this.WindowStyle = WindowStyle.None;
                this.ResizeMode = ResizeMode.NoResize;
                width = this.Width;
                heigth = this.Height;
                this.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
                this.Height = System.Windows.SystemParameters.PrimaryScreenHeight;

                // Get Left and Top of current screen.
                var screen = System.Windows.Forms.Screen.FromHandle(new System.Windows.Interop.WindowInteropHelper(this).Handle);
                this.Left = screen.WorkingArea.Left;
                this.Top = screen.WorkingArea.Top;
                this.WindowState = WindowState.Normal;

                hideChat();
                hideStreamsList();

                playerControlsColumn.Height = new GridLength(0);
                hide1.Width = new GridLength(0);
                hide2.Width = new GridLength(0);
            }
            else
            {
                this.WindowState = WindowState.Maximized;
                this.WindowStyle = WindowStyle.SingleBorderWindow;
                this.ResizeMode = ResizeMode.CanResize;
                this.Width = width;
                this.Height = heigth;

                playerControlsColumn.Height = new GridLength(28);
                hide1.Width = new GridLength(15);
                hide2.Width = new GridLength(15);

                showChat();
                showStreamsList();
            }
        }

        private void volumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (vlcPlayer != null)
                vlcPlayer.AudioProperties.Volume = Convert.ToInt32(volumeSlider.Value);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (vlcPlayer != null)
                {
                    vlcPlayer.Dispose();
                    vlcPlayer = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void qualityComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!streamSelected)
            {
                quality = ((Stream)qualityComboBox.SelectedItem).url;
                vlcPlayer.Media = new Vlc.DotNet.Core.Medias.PathMedia(quality);
            }
            else
            {
                streamSelected = false;
            }
        }

        private void playButton_Click(object sender, RoutedEventArgs e)
        {
            if (vlcPlayer != null)
            {
                pauseButton.Visibility = Visibility.Visible;
                playButton.Visibility = Visibility.Collapsed;
                vlcPlayer.Pause();
            }
        }

        private void pauseButton_Click(object sender, RoutedEventArgs e)
        {
            if (vlcPlayer != null)
            {
                pauseButton.Visibility = Visibility.Collapsed;
                playButton.Visibility = Visibility.Visible;
                vlcPlayer.Pause();
            }
        }

        private void fullScreenButton_Click(object sender, RoutedEventArgs e)
        {
            toggleFullScreen();
        }

        private void volumeButton_Click(object sender, RoutedEventArgs e)
        {
            volumeButton.Visibility = Visibility.Collapsed;
            muteButton.Visibility = Visibility.Visible;
            vlcPlayer.AudioProperties.Volume = 0;
        }

        private void muteButton_Click(object sender, RoutedEventArgs e)
        {
            volumeButton.Visibility = Visibility.Visible;
            muteButton.Visibility = Visibility.Collapsed;
            vlcPlayer.AudioProperties.Volume = Convert.ToInt32(volumeSlider.Value);
        }

        /// <summary>
        /// Hides script error in WebBrowser
        /// </summary>
        /// <param name="wb"></param>
        /// <param name="Hide"></param>
        public void HideScriptErrors(WebBrowser wb, bool Hide)
        {
            FieldInfo fiComWebBrowser = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return;
            object objComWebBrowser = fiComWebBrowser.GetValue(wb);
            if (objComWebBrowser == null) return;
            objComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { Hide });
        }

        private void webBrowser1_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            HideScriptErrors(webBrowser1, true);
        }
    }
}


