﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TwitchPlayer
{
    class Stream
    {
        public string url { get; set; }
        public string name { get; set; }

        public Stream(string url, string name)
        {
            this.url = url;
            this.name = name;
        }
    }
}
