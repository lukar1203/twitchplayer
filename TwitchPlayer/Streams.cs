﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TwitchPlayer
{
    public class Streams
    {
        public string game { get; set; }
        public long id { get; set; }
        public long viewers { get; set; }
        public Channel channel { get; set; }
        public string _links { get; set; }
    }
}
