﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TwitchPlayer
{
    public class Top
    {
        public int viewers { get; set; }
        public int channels { get; set; }
        public Game game { get; set; }
    }
}
